# -*- coding: utf-8 -*-
#   This file is part of PyMSES.
#
#   PyMSES is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   PyMSES is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with PyMSES.  If not, see <http://www.gnu.org/licenses/>.
r"""
:mod:`pymses.sources.ramses.sources` --- RAMSES data sources module
-------------------------------------------------------------------

"""
from pymses.core import DataSource


class RamsesGenericSource(DataSource):
    r"""
    RAMSES generic data source

    Parameters
    ----------
    reader: TODO
    dom_decomp: TODO
    cpu_list:``list`` of ``int``
        list of cpu index
    ndim: ``int``
        number of dimensions
    field_list: ``list`` of ``string``
        list of fieldd names to read
    """
    def __init__(self, reader, dom_decomp, cpu_list, ndim, field_list):
        super(RamsesGenericSource, self).__init__(dom_decomp, cpu_list, field_list, ndim=ndim)
        self.reader = reader

    def get_domain_dset(self, icpu, verbose=None):
        r"""
        Data source reading method

        Parameters
        ----------
        icpu : ``int``
            CPU file number to read
        verbose: ``bool`` or None.
            verbosity boolean flag. Default None.

        Returns
        -------
        dset : ``AbstractDataset``
            the dataset containing the data from the given cpu number file

        """
        return self.reader.read_file(icpu, self.get_read_levelmax(), verbose=verbose)


class RamsesAmrSource(RamsesGenericSource):
    r"""
    RAMSES AMR data source class

    """

    def __init__(self, reader, dom_decomp=None, cpu_list=None, ndim=None, field_list=None,
                 inv_coarse_gvals=False):
        super(RamsesAmrSource, self).__init__(reader, dom_decomp, cpu_list, ndim, field_list)
        self._invalid_coarse_grid_values = inv_coarse_gvals

    def source_type(self):
        r"""
        Returns
        -------
        t: DataSource.AMR_SOURCE

        """
        return DataSource.AMR_SOURCE

    def is_invalid_coarse_grid_values(self):
        """
        Returns
        -------
        b: ``bool``
            True when levelmin==levelmax. WARNING : AMR field values for ilevel<levelmin corresponds to initial
            conditions and are not updated by Ramses afterwards !!! Otherwise return False
        """
        return self._invalid_coarse_grid_values


class RamsesParticleSource(RamsesGenericSource):
    r"""
    RAMSES particle data source class

    """

    def __init__(self, reader, dom_decomp=None, cpu_list=None, ndim=None, field_list=None):
        super(RamsesParticleSource, self).__init__(reader, dom_decomp, cpu_list, ndim, field_list)

    def source_type(self):
        r"""
        Returns
        -------
        DataSource.PARTICLE_SOURCE

        """
        return DataSource.PARTICLE_SOURCE


__all__ = ["RamsesGenericSource", "RamsesAmrSource", "RamsesParticleSource"]
