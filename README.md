# PyMSES 4.2 package

This Python 2 package is intended for RAMSES magneto-hydrodynamical code data analysis and processing  to help RAMSES users manipulate their RAMSES data without them knowing its inner format/structure.

## INSTALLATION INSTRUCTIONS

0. If you use the developer version, you need to generate the cython files first :

```shell
$ make cython
```

1. Build the compiled modules

   You can either do:

   ```shell
   $ make
   ```

   to perform an inplace build with default compiler settings, or

   ```shell
   $ python setup.py
   ```

   to use the interactive distutil build utility (e.g. to customize the compiler settings). Choose option 3, inplace build.

2. Ensure the root PyMSES directory is in your **$PYTHONPATH** environment variable.

3. (optional). Test the installation and code using

   ```shell
   $ make test
   ```

### Notes for Apple MAC OS X users

`src/fio.c` source file require a `byteswap.h` library to read RAMSES Fortran90 binary format and this lib might be unavailable in your distribution.

1. Try to replace the import type `#include <byteswap.h>` by `#include "byteswap.h"` in `src/fio.c`

2. Additional informations :
 - You may locate such a `byteswap.h` lib in your distribution: just copy it into the src/ directory
 - Or you can dowload it, see the fink page : http://pdb.finkproject.org/pdb/package.php/byteswap-dev
 
## Release notes

### PyMSES v4.1.5
- pymses.utils.constants module with Unit : added name and unit type management
- byteswapping management
- particle metallicity management in low-level C I/O routines
- pymses run level changes configuration
- ramses output file field abstraction layer

### PyMSES v.4.0.0
- Improved GUI
- Some more bug fixes

### PyMSES v.3.1.0 
- Improved GUI
- Added OpenCL possibility for the bottom up OctreeRayTracer
- Some more bug fixes

### PyMSES v.3.0.0
- Second public release of the PyMSES module.
- Added some bug fixes Improved multiprocessing performance
- Added mpi4py ray tracer
- Added RAMSES user defined AMR file formats
- Added the new OctreeRayTracer in the visualisation module

### PyMSES v.2.1.0
- First public release of the PyMSES module.
- Enables the AMRViewer GUI
 